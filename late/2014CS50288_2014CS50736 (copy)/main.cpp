#include<iostream>
#include<cstring>
#include<string>
#include<fstream>

#include<vector>

#define max 10000

using namespace std;

vector<bool> vertexG1(max,0); //1 or 0 array
unsigned short sizeG1=0;
vector<bool> vertexG2(max,0);
unsigned short sizeG2=0;
vector<vector<bool>> edgeG1(max,vector<bool>(max,0)); //1 or 0 array
unsigned int sizeEg1=0;
vector<vector<bool>> edgeG2(max,vector<bool>(max,0));
unsigned int sizeEg2=0;
vector<vector<unsigned int>> encoding(max,vector<unsigned int>(max,0));
vector<unsigned short> indegreeG1(max,0); 
vector<unsigned short> indegreeG2(max,0);
vector<unsigned short> outdegreeG1(max,0); 
vector<unsigned short> outdegreeG2(max,0);
vector<unsigned int> sumindegreeG1(max,0);
vector<unsigned int> sumindegreeG2(max,0);
vector<unsigned int> sumoutdegreeG1(max,0); 
vector<unsigned int> sumoutdegreeG2(max,0);
vector<vector<bool>> eligibleG1(max,vector<bool>(max,0));
vector<vector<bool>> eligibleG2(max,vector<bool>(max,0));
vector<unsigned short> maxinneighbourG1(max,0);
vector<unsigned short> maxinneighbourG2(max,0);
vector<unsigned short> maxoutneighbourG1(max,0);
vector<unsigned short> maxoutneighbourG2(max,0);
unsigned int totalclauses=0;
unsigned int var=0;
unsigned int code=1;

ofstream fout;
ofstream foute("encoding");
void getMap();
void eligibleMaps();
void noMap();
void neighbourHeuristic();

int main(int argc, char *argv[]) {
	
	//reading input from graphinput
	int a,b;
	
	char file[100];
	strcpy(file,argv[1]);
	
	ifstream fin(strcat(file,".graphs"));
	fin>>a>>b;
	
	fout.open(strcat(argv[1],".satinput"));

	while(a!=0 || b!=0) {
		
		if(a>sizeG2){
			
			sizeG2=a;
		}
		if(b>sizeG2){
			sizeG2=b;
		}
		vertexG2[a] = 1;
		vertexG2[b] = 1;
		edgeG2[a][b]=1;
		outdegreeG2[a]++;
		indegreeG2[b]++;
		sizeEg2++;
		fin>>a>>b;
		
	}
	
	while(fin>>a>>b) {
		
		if(a>sizeG1){
			sizeG1=a;
		}
		if(b>sizeG1){
			sizeG1=b;
		}
		vertexG1[a] = 1;
		vertexG1[b] = 1;
		edgeG1[a][b]=1;
		outdegreeG1[a]++;
		indegreeG1[b]++;
        sizeEg1++;
	}
	
	fin.close();
	
	
	//eligible according to degree constraint
	eligibleMaps();
	
	//if eligible vertices for any vertex in G1 is 0 then there cannot be any mapping
	noMap();
	
	//neighbour heuristic
	neighbourHeuristic();
	
	//complex clauses
	getMap();

	fout.close();	
	

}


//complex clause type 3
void getMap() {
	
	//orring AP or AQ or AR
	for(int i=1; i<=sizeG1; i++){
		
		for(int j=1; j<=sizeG2; j++){
            
            if(eligibleG1[i][j]==1){

				foute<<i<<'e'<<j<<" "<<code<<endl;
				fout<<code<<" ";
				encoding[i][j]=code;
				code++;
				var++;
			}
				
		}
		fout<<0<<endl;
		totalclauses++;
	}
    foute.close();
    

    //xorring AP xor AQ
	for(int i=1; i<=sizeG1;i++){
		
		for(int j=1; j<=sizeG2;j++){
			
			if(eligibleG1[i][j]==1){
				for(int m=j+1; m<=sizeG2; m++){
				    
				    if(eligibleG1[i][m]==1){
						
						fout<<"-"<<encoding[i][j]<<" -"<<encoding[i][m]<<" 0"<<endl;
						totalclauses++;
					}
				    
			    }
			}

		}
	}
	
	
	//xorring AP xor BP
	for(int j=1; j<=sizeG2;j++){
		
		for(int i=1; i<=sizeG1;i++){
			
			if(eligibleG2[j][i]==1){
				for(int m=i+1; m<=sizeG1; m++){
				    
				    if(eligibleG2[j][m]==1){
						fout<<"-"<<encoding[i][j]<<" -"<<encoding[m][j]<<" 0"<<endl;
						totalclauses++;
						
					}
				    
			    }
			}

		}
	}

    
    //most complex clauses
	for(int i=1; i<=sizeG1; i++){ //A
		cout<<i<<endl;
		for(int j=1; j<=sizeG1; j++){ //B
			
			if(i<j){
				
				for(int k=1; k<=sizeG2; k++){ //P
					
					if(eligibleG1[i][k]==1){
						
						for(int m=1; m<=sizeG2; m++){  //Q
							
							if(eligibleG1[j][m]==1 && k!=m && (edgeG1[i][j]!=edgeG2[k][m] || edgeG1[j][i]!=edgeG2[m][k])){

							    //fout<<"-"<<i<<777<<k<<" -"<<j<<777<<m<<" -"<<i<<333<<j<<" "<<k<<555<<m<<" 0"<<endl;
							    fout<<"-"<<encoding[i][k]<<" -"<<encoding[j][m]<<" 0"<<endl;
							
							    //fout<<"-"<<i<<777<<k<<" -"<<j<<777<<m<<" "<<i<<333<<j<<" -"<<k<<555<<m<<" 0"<<endl;
							
							    //fout<<"-"<<i<<777<<k<<" -"<<j<<777<<m<<" -"<<j<<333<<i<<" "<<m<<555<<k<<" 0"<<endl;
							
							    //fout<<"-"<<i<<777<<k<<" -"<<j<<777<<m<<" "<<j<<333<<i<<" -"<<m<<555<<k<<" 0"<<endl;
							    totalclauses++; //changed

							}
						    
					    }
						
					}
					
				}
				
				
			}
		}
	}
	
}


void eligibleMaps() {
	
	for(int i=1; i<=sizeG1; i++){

		int indegreeg1 = indegreeG1[i];
		int outdegreeg1 = outdegreeG1[i];
		
		for(int j=1; j<=sizeG2; j++){
			
			int indegreeg2 = indegreeG2[j];
			int outdegreeg2 = outdegreeG2[j];
			
			if(indegreeg1<=indegreeg2 && outdegreeg1<=outdegreeg2){  
				
				eligibleG1[i][j] = 1;
				eligibleG2[j][i] = 1;
			}
			
		}
	}
	
}

void noMap(){
	
	if(sizeG2<sizeG1 or sizeEg2<sizeEg1){
		fout<<"p cnf "<<0<<" "<<0;
		fout.close();
		exit(0);
	}
	
	for(int i=1; i<=sizeG1; i++){
		
		bool isvalid = false;
		for(int j=1; j<=sizeG2; j++){
			
			if(eligibleG1[i][j]==1){
				isvalid = true;
				break;
			}
		}
		
		if(isvalid==false){
  
			fout<<"p cnf "<<0<<" "<<0;
			fout.close();
			exit(0);
		}
		
	}
	
}

void neighbourHeuristic(){
	
	int maxin = 0;
	int maxout = 0;
	int sumindegree=0;
	int sumoutdegree=0;
	
	for(int i=1; i<=sizeG1; i++){
		sumindegree=0;
		sumoutdegree=0;
		maxin=0;
		maxout=0;
		for(int j=1; j<=sizeG1; j++){
			
			if(edgeG1[i][j]==1){
				if(indegreeG1[j]>maxin){
					maxin = indegreeG1[j];
				}
				if(outdegreeG1[j]>maxout){
					maxout = outdegreeG1[j];
				}
				sumindegree += indegreeG1[j];
				sumoutdegree += outdegreeG1[j];
			}
		}
		sumindegreeG1[i]=sumindegree;
		sumoutdegreeG1[i]=sumoutdegree;
		maxinneighbourG1[i] = maxin;
		maxoutneighbourG1[i] = maxout;
	}
	
	for(int i=1; i<=sizeG2; i++){
		sumindegree=0;
		sumoutdegree=0;
		maxin=0;
		maxout=0;
		for(int j=1; j<=sizeG2; j++){
			
			if(edgeG2[i][j]==1){
				if(indegreeG2[j]>maxin){
					maxin = indegreeG2[j];
				}
				if(outdegreeG2[j]>maxout){
					maxout = outdegreeG2[j];
				}
				sumindegree += indegreeG2[j];
				sumoutdegree += outdegreeG2[j];
			}
		}
		sumindegreeG2[i]=sumindegree;
		sumoutdegreeG2[i]=sumoutdegree;
		maxinneighbourG2[i] = maxin;
		maxoutneighbourG2[i] = maxout;
	}
	
	int sumindegreeg1;
	int sumoutdegreeg1;
	int sumindegreeg2;
	int sumoutdegreeg2;

	for(int i=1; i<=sizeG1; i++){

		sumindegreeg1 = sumindegreeG1[i];
		sumoutdegreeg1 = sumoutdegreeG1[i];
		
		for(int j=1; j<=sizeG2; j++){
			
		    sumindegreeg2 = sumindegreeG2[j];
			sumoutdegreeg2 = sumoutdegreeG2[j];
			
			if(sumindegreeg1>sumindegreeg2 || sumoutdegreeg1>sumoutdegreeg2){  
                
				
				eligibleG1[i][j] = 0;
				eligibleG2[j][i] = 0;
			}
			
		}
	}
	
	int maxindegreeg1;
	int maxoutdegreeg1;
	int maxindegreeg2;
	int maxoutdegreeg2;
	

	for(int i=1; i<=sizeG1; i++){

		maxindegreeg1 = maxinneighbourG1[i];
		maxoutdegreeg1 = maxoutneighbourG1[i];
		
		for(int j=1; j<=sizeG2; j++){
			
			maxindegreeg2 = maxinneighbourG2[j];
			maxoutdegreeg2 = maxoutneighbourG2[j];
			
			if(maxindegreeg1>maxindegreeg2 || maxoutdegreeg1>maxoutdegreeg2){  
                
                
				eligibleG1[i][j] = 0;
				eligibleG2[j][i] = 0;
			}
			
		}
	}

}


