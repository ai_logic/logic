#include<iostream>
#include<cstring>
#include<string>
#include<fstream>
#include<vector>
#include<unordered_map>

#define CODEWORD "e"

using namespace std;

unordered_map<int,string> encoding;
vector<string> decoding;
bool exist=true;

int main(int argc, char *argv[]){
	
	//reading input
	ifstream fin("encoding");

	
	string line;
	int code;
	while(fin>>line){
		if(line=="") {
			cout<<"empty encoding";
			exist=false;
			break;
		}
		
		fin>>code;
		encoding.insert(make_pair(code,line));
		//cout<<code<<" "<<line<<"\n";
	}
	fin.close();
	
	char file[100];
	strcpy(file,argv[1]);
	
	fin.open(strcat(file,".satoutput"));
	
	fin>>line;
	if(line=="UNSAT") {
		exist = false;
	}
	
	fin>>code;
	if(code==0){
		exist = false;
	}
	
	ofstream fout(strcat(argv[1],".mapping"));
	
	if(exist==false){
		fout<<0;
		fout.close();
		return 0;
	}
	
	string g;
	
	while (code) {
		
		if(code>0) {
			auto got = encoding.find(code);
			
			g = got->second;

			//cout<<g<<"\n";
			size_t found = g.find(CODEWORD);
			
			fout<<g.substr(0,found)<<" "<<g.substr(found+1)<<"\n";
		}
		fin>>code;
	}
	
	fin.close();
	
	fout.close();

}
