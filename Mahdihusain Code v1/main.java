import java.util.Vector;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Hashtable;
import java.io.PrintWriter;

public class main{
	
	public static Vector<String> vertexG1 = new Vector<String>();
	public static Vector<literal> edgeG1 = new Vector<literal>();
	public static Vector<String> vertexG2 = new Vector<String>();
	public static Vector<literal> edgeG2 = new Vector<literal>();
	
	public static Hashtable<String,Integer> encoding = new Hashtable<String,Integer>();  
	public static Hashtable<String,Boolean> isVpresent = new Hashtable<String,Boolean>();
	public static Hashtable<String,Boolean> isEpresent = new Hashtable<String,Boolean>();
	public static Vector<Vector<String>> per = new Vector<Vector<String>>();
	public static Integer code = 1;
	
	public static cnf basicCNF = new cnf();
	
	public static void main(String args[]){
		
		long start = System.currentTimeMillis();
		
		//reading input from graphinput
		try (BufferedReader br = new BufferedReader(new FileReader("graphinput"))) {
			
            String line;  
            boolean isG2 = true;
            String[] edge = new String[2];
            
            while ((line = br.readLine()) != null) {
                
                edge = line.split(" ");
                
                if(edge[0].equals("0") && edge[1].equals("0")){
					isG2 = false;
				}else if(isG2){
					addVertex("b" + edge[0] ,false);
					addVertex("b" + edge[1],false);
					literal l = new literal("b"+edge[0],"b"+edge[1],false);
					edgeG2.add(l);
					isEpresent.put("b"+edge[0]+"b"+edge[1], true);
				}else{
					addVertex("s"+edge[0],true);
					addVertex("s"+edge[1],true);
                    literal l = new literal("s"+edge[0],"s"+edge[1],false);
					edgeG1.add(l);
					isEpresent.put("s"+edge[0]+"s"+edge[1], true);
				}
                
            }        
        }catch(Exception e){
			System.out.println("File reading exception");
		}
		
		//adding graph 1 edges in cnf 
		for(int i=0; i<vertexG1.size();i++){
			for(int j=0; j<vertexG1.size();j++){
				if(i!=j){
					clause c = new clause();
					literal l = new literal(vertexG1.get(i),vertexG1.get(j),false);
				
					if(isEpresent.get(vertexG1.get(i)+vertexG1.get(j))!=null){
						c.addLiteral(l);
					}else{
						l.isneg = true;
						c.addLiteral(l);
					}
				
					basicCNF.addClause(c);
				}
			}
		}
		
		//adding graph 2 edges in cnf
		for(int i=0; i<vertexG2.size();i++){
			for(int j=0; j<vertexG2.size();j++){
				if(i!=j){
					clause c = new clause();
					literal l = new literal(vertexG2.get(i),vertexG2.get(j),false);
					
					if(isEpresent.get(vertexG2.get(i)+vertexG2.get(j))!=null){
						c.addLiteral(l);
					}else{
						l.isneg = true;
						c.addLiteral(l);
					}
					
					basicCNF.addClause(c);
				}
			}
		}
		
		//add cnf of mapping(one-one) to basic cnf
		getMap(vertexG1,vertexG2);
		System.out.println("one-one clauses");
		
		//add constraint cnf to basic cnf
		constraintCNF(vertexG1,vertexG2);
		System.out.println("constraint cnf added");
		System.out.println("Time to encode: " + (System.currentTimeMillis()-start) + " millisecs");
		
		//write output to the file
		try{
		    PrintWriter writer = new PrintWriter("satinput");
		    PrintWriter writere = new PrintWriter("encoding");
		    
		    for(int i=0; i<basicCNF.cls.size(); i++){
			    for(int j=0; j<basicCNF.cls.get(i).lits.size(); j++){
				
				    String fromto = basicCNF.cls.get(i).lits.get(j).from + basicCNF.cls.get(i).lits.get(j).to;
				
				    if(encoding.containsKey(fromto)==false){
					    encoding.put(fromto,code);
					    writere.println(fromto + " " + code);
					    code++;
					}
				
			    }
			    
		    }
		    
		    writer.println("p cnf " + (code-1) + " " + basicCNF.cls.size());
		    
		    for(int i=0; i<basicCNF.cls.size(); i++){
				
				String line = "";
				
			    for(int j=0; j<basicCNF.cls.get(i).lits.size(); j++){
				
				    String fromto = basicCNF.cls.get(i).lits.get(j).from + basicCNF.cls.get(i).lits.get(j).to;
				
				    if(basicCNF.cls.get(i).lits.get(j).isneg){
						line += "-" + encoding.get(fromto) + " ";
				    }else{
					    line += encoding.get(fromto) + " ";
				    }
			    }
			    line += "0";
			    writer.println(line);
			    line = "";
		    }
		    
		    writer.close();
		    writere.close();
		    
	    }catch(Exception e){
			System.out.println("File writing exception");
		}
		
		//System.out.println(encoding.toString());
		System.out.println("CNFsize: " + basicCNF.cls.size());
		//time complexity
		long end = System.currentTimeMillis();
		System.out.println("Time to encode: " + (end-start) + " millisecs");
		long seconds = (end-start)/1000;
		System.out.println("Time to encode: " + seconds + " secs");
		
	}
	
	//permutation
	public static void permutation(Vector<String> vleft, Vector<String> vfilled, int n){
		
		Vector<String> vleftcopy = new Vector<String>(vleft);
		Vector<String> vfilledcopy = new Vector<String>(vfilled);
		
		if(n==vfilled.size()){
			per.add(vfilled);
			return;
		}
		
		int limit = vleft.size();
		
		for(int i=0; i<limit; i++){
			String rem = vleft.get(i);
			vfilled.add(rem);
			vleft.remove(i);
			permutation(vleft,vfilled,n);
			vfilled = new Vector<String>(vfilledcopy);
			vleft = new Vector<String>(vleftcopy);
		}
	}
	
	//partial cnf
	public static cnf partialCNF(Vector<String> vertexG1, Vector<String> vertexG2){
		
		Vector<String> tofill = new Vector<String>();
		permutation(vertexG2, tofill, vertexG1.size());
        
        System.out.println("permutation size: " + per.size());
		
		cnf pCNF = new cnf();
		
		for(int i=0; i<per.size(); i++){
			clause c = new clause();

			for(int j=0; j<per.get(i).size(); j++){
				literal l = new literal(vertexG1.get(j),per.get(i).get(j),true);
				c.addLiteral(l);
			}
			
			pCNF.addClause(c);
		}
		
		return pCNF;
	}
	
	//total cnf 
	public static void constraintCNF(Vector<String> vertexG1, Vector<String> vertexG2){
		
		cnf pCNF = partialCNF(vertexG1, vertexG2);
		System.out.println("partial cnf done");
		
		for(int i=0; i<pCNF.cls.size(); i++){
			completeClause((pCNF.cls.get(i)));
		}
	}
	
	//completing partially filled clause
	public static void completeClause(clause c){
		
		for(int i=0; i<c.lits.size(); i++){
			for(int j=0; j<c.lits.size(); j++){
				if(i<j){
					
					String ifrom = c.lits.get(i).from; //A
					String ito = c.lits.get(i).to;  //P
					String jfrom = c.lits.get(j).from; //B
					String jto = c.lits.get(j).to; //Q
					
					
					clause cnew1 = new clause();
					literal l1 = new literal(ifrom,jfrom,true);
					literal l2 = new literal(ito,jto,false);
					
					cnew1.addClause(c);
					cnew1.addLiteral(l1);
					cnew1.addLiteral(l2);
					
					basicCNF.addClause(cnew1);
					
					clause cnew2 = new clause();
					literal l3 = new literal(ifrom,jfrom,false);
					literal l4 = new literal(ito,jto,true);
					
					cnew2.addClause(c);
					cnew2.addLiteral(l3);
					cnew2.addLiteral(l4);
					
					basicCNF.addClause(cnew2);
					
					clause cnew3 = new clause();
					literal l5 = new literal(jfrom,ifrom,true);
					literal l6 = new literal(jto,ito,false);
					
					cnew3.addClause(c);
					cnew3.addLiteral(l5);
					cnew3.addLiteral(l6);
					
					basicCNF.addClause(cnew3);
					
					clause cnew4 = new clause();
					literal l7 = new literal(jfrom,ifrom,false);
					literal l8 = new literal(jto,ito,true);
					
					cnew4.addClause(c);
					cnew4.addLiteral(l7);
					cnew4.addLiteral(l8);
					
					basicCNF.addClause(cnew4);
				}
			}
		}
	}
	
	//ensuring everyone gets mapped xor
	public static void getMap(Vector<String> vertexG1, Vector<String> vertexG2){
		
		for(int i=0; i<vertexG1.size(); i++){
			clause c = new clause();
			String from = vertexG1.get(i);
			
			for(int j=0; j<vertexG2.size(); j++){
				String to = vertexG2.get(j);
				literal l1 = new literal(from,to,false);
				c.addLiteral(l1);
			}
			
            basicCNF.addClause(c);
		}
	
		for(int j=0; j<vertexG1.size();j++){
            String from = vertexG1.get(j);
				
			for(int k=0; k<vertexG2.size();k++){
				String to = vertexG2.get(k);
					
				for(int m=k+1; m<vertexG2.size(); m++){
					clause c = new clause();
					String tom = vertexG2.get(m);
					literal l1 = new literal(from,to,true);
					literal l2 = new literal(from,tom,true);
					c.addLiteral(l1);
					c.addLiteral(l2);
					basicCNF.addClause(c);
				}	
			}
		}
		
		for(int j=0; j<vertexG2.size();j++){
			String to = vertexG2.get(j);
				
			for(int k=0; k<vertexG1.size();k++){
				String from = vertexG1.get(k);
					
				for(int m=k+1; m<vertexG1.size(); m++){
					clause c = new clause();
					String fromm = vertexG1.get(m);
					literal l1 = new literal(from,to,true);
					literal l2 = new literal(fromm,to,true);
					c.addLiteral(l1);
					c.addLiteral(l2);
					basicCNF.addClause(c);
				}		
			}
		}
	}
	
	public static void addVertex(String s, boolean isG1){
		
		if(isG1){
			if(isVpresent.get(s)==null){
				vertexG1.add(s);
				isVpresent.put(s,true);
			}
		}else{
			if(isVpresent.get(s)==null){
				vertexG2.add(s);
				isVpresent.put(s,true);
			}
		}
		
	}
	
}
































