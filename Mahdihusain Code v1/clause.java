import java.util.Vector;
public class clause{
	
	public Vector<literal> lits;
	
	public clause(){
		this.lits = new Vector<literal>();
	}
	
	public void addLiteral(literal l){
		this.lits.add(l);
	}
	
	public void addClause(clause c){
		
		for(int i=0; i<c.lits.size(); i++){
			addLiteral(c.lits.get(i));
		}
	}
}
