import java.util.Vector;
public class cnf{
	
	public Vector<clause> cls;
	
	public cnf(){
		this.cls = new Vector<clause>();
	}
	
	public void addCNF(cnf cn){
		
		for(int i=0; i<cn.cls.size(); i++){
			this.cls.add(cn.cls.get(i));
		}
	}
	
	public void addClause(clause c){
		this.cls.add(c);
	}
	
}
