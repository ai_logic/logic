public class literal{
	
	public String from;
	public String to;
	public boolean isneg;
	
	public literal(String from, String to, boolean isneg){
		this.from = from;
		this.to = to;
		this.isneg = isneg;
	}
	
	@Override
	public String toString(){
		return this.from + this.to;
	}
	
	@Override
	public boolean equals(Object o){
		
		if (o == this) {
		
            return true;
        }
		
		if (!(o instanceof literal)) {
            return false;
        }
		
		literal t = (literal) o;
		
		if(t.from.equals(this.from) && t.to.equals(this.to) && t.isneg==this.isneg){
			return true;
		}
		return false;
	}
}
