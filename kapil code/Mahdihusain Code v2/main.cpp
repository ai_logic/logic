#include<iostream>
#include<string>
#include<fstream>
#include<vector>
#include<unordered_map>
#include<unordered_set>
#include <algorithm>
#include <cstdlib>
using namespace std;

void eligibleMaps();
void getMap();
void constraintCNF();
void noMap();
void heuristic();

struct literal {
	string from;
	string to;
	bool isneg;
};

struct tupl {
	int indegree;
	int outdegree;
};

struct clause {
	vector<literal> lits = vector<literal>();
	unordered_set<string> litsg2 = unordered_set<string>();
};

struct cnf {
	vector<clause> cls = vector<clause>();
};

vector<string> vertexG1, vertexG2;
vector<literal> edgeG1, edgeG2;

unordered_map<string,int> encoding;
unordered_map<string,tupl> isVpresent;
unordered_map<string,bool> isEpresent;
unordered_map<string,int> heuristicg2;
int code = 1;

unordered_map<string,vector<string>> eligible;
unordered_map<string,vector<string>> eligibleg2;

cnf basicCNF;

int main() {
	
	//reading input from graphinput
	string a,b;
	ifstream fin("graphinput");
	fin>>a>>b;
	
	tupl t;
	string s;
	// unordered_map<string,tupl>::const_iterator got;
	while(a!="0" || b!="0") {
		
		s="b"+a;
		auto got = isVpresent.find(s);
		if (got == isVpresent.end()) {
			t = {0,1};
			vertexG2.push_back(s);
			vector<string> v;
			isVpresent.insert(make_pair(s,t));
			eligibleg2.insert(make_pair(s,v));
		}
		else {
			(got->second).outdegree++;
		}
		
		
		s="b"+b;
		got = isVpresent.find(s);
		if (got == isVpresent.end()) {
			t = {1,0};
			vertexG2.push_back(s);	
			isVpresent.insert(make_pair(s,t));
			vector<string> v;
			eligibleg2.insert(make_pair(s,v));
		}
		else {
			(got->second).indegree++;
		}
		
		
		literal l = {"b"+a,s,false};
		edgeG2.push_back(l);
		isEpresent.insert(make_pair("b"+a+s, true));
		
		fin>>a>>b;
	}
	
	
	while(fin>>a>>b) {
		
		s = "s"+a;
		auto got = isVpresent.find(s);
		if (got == isVpresent.end()) {
			t = {0,1};
			vertexG1.push_back(s);
			vector<string> v;
			isVpresent.insert(make_pair(s,t));
			eligible.insert(make_pair(s,v));
		}
		else {
			(got->second).outdegree++;
		}
		
		
		s="s"+b;
		got = isVpresent.find(s);
		if (got == isVpresent.end()) {
			t = {1,0};
			vertexG1.push_back(s);	
			vector<string> v;
			eligible.insert(make_pair(s,v));
			isVpresent.insert(make_pair(s,t));
		}
		else {
			(got->second).indegree++;
		}
		
		
		literal l = {"s"+a,s,false};
		edgeG2.push_back(l);
		isEpresent.insert(make_pair("s"+a+s, true));
	}
	
	fin.close();
	
	//adding graph 1 edges in cnf 
	unordered_map<string,bool>::const_iterator got2;
	for(int i=0; i<vertexG1.size();i++){
		for(int j=0; j<vertexG1.size();j++){
			if(i!=j){
				clause c;
				literal l = {vertexG1[i],vertexG1[j],false};
				
				got2 = isEpresent.find(vertexG1[i]+vertexG1[j]);
				if (got2 != isEpresent.end()) {
					c.lits.push_back(l);
				}
				else {
					l.isneg = true;
					c.lits.push_back(l);
				}
			
				basicCNF.cls.push_back(c);
			}
		}
	}
	
	//cout<<"no. of clauses due to edge in g1 "<<basicCNF.cls.size()<<endl;
	
	
	//adding graph 2 edges in cnf
	for(int i=0; i<vertexG2.size();i++){
		for(int j=0; j<vertexG2.size();j++){
			if(i!=j){
				clause c;
				literal l = {vertexG2[i],vertexG2[j],false};
				
				got2 = isEpresent.find(vertexG2[i]+vertexG2[j]);
				if(got2 != isEpresent.end()){
					c.lits.push_back(l);
				}else{
					l.isneg = true;
					c.lits.push_back(l);
				}
				
				basicCNF.cls.push_back(c);
			}
		}
	}
	
	//cout<<"no. of clauses due to edge in g1 + g2"<<basicCNF.cls.size()<<endl;
	
	//eligible
	eligibleMaps();
	
	noMap();
	cout<<"no. of clauses due to edge in g1 + g2 + one-one clauses "<<basicCNF.cls.size()<<endl;
	// heuristic();
	//add cnf of mapping(one-one) to basic cnf
	getMap();
	cout<<"no. of clauses due to edge in g1 + g2 + one-one clauses "<<basicCNF.cls.size()<<endl;
	
	//add constraint cnf to basic cnf
	// constraintCNF();
	cout<<"constraint cnf added\n";
	// System.out.println("Time to encode: " + (System.currentTimeMillis()-start) + " millisecs");
	
	//write output to the file
	ofstream fout("satinput");
	ofstream foute("encoding");
	
	unordered_map<string,int>::const_iterator got1;
	for(int i=0; i<basicCNF.cls.size(); i++){
		for(int j=0; j<basicCNF.cls[i].lits.size(); j++){
		
			string fromto = basicCNF.cls[i].lits[j].from + basicCNF.cls[i].lits[j].to;
			
			got1 = encoding.find(fromto);
			if(got1 == encoding.end()){
				encoding.insert(make_pair(fromto,code));
				foute<<fromto<<" "<<code<<"\n";
				code++;
			}
		
		}
		
	}
	
	fout<<"p cnf "<<(code-1)<<" "<<basicCNF.cls.size()<<"\n";
	
	for(int i=0; i<basicCNF.cls.size(); i++){
		
		string line = "";
		
		for(int j=0; j<basicCNF.cls[i].lits.size(); j++){
		
			string fromto = basicCNF.cls[i].lits[j].from + basicCNF.cls[i].lits[j].to;
		
			got1 = encoding.find(fromto);
			if(basicCNF.cls[i].lits[j].isneg){
				line += ("-" + to_string(got1->second) + " ");
			}else{
				line += (to_string(got1->second) + " ");
			}
		}
		line += "0";
		fout<<line<<"\n";
	}
	
	fout.close();
	foute.close();
	
	cout<<"CNFsize: "<<basicCNF.cls.size()<<"\n";
	
}



cnf secondpartialCNF(int index, cnf c){
	unordered_map<string,vector<string>>::const_iterator got;
	unordered_set<string>::const_iterator got2;
	cnf ret;
	
	if(index>=vertexG1.size()){
		return c;
	}
	
	cnf cp;
	vector<clause> temp(c.cls);
	cp.cls.swap(temp);

	string s = vertexG1[index];
	got = eligible.find(s);
	vector<string> v = got->second;
	
	for(int j=0; j<v.size(); j++){
		literal l = {s,v[j],true};
		
		for(int k=0; k<cp.cls.size(); k++){
			got2 = cp.cls[k].litsg2.find(v[j]);
			if(got2 != cp.cls[k].litsg2.end()){
				cp.cls.erase(cp.cls.begin() + k);
				k--;
				continue;
			}

			cp.cls[k].lits.push_back(l);	
			cp.cls[k].litsg2.insert(v[j]);	
		}
		
		vector<clause> temp1(secondpartialCNF(index+1,cp).cls);
		for(int i=0; i<temp1.size(); i++){
			ret.cls.push_back(temp1[i]);
		}
		
        
        vector<clause> temp2(c.cls);
        cp.cls.swap(temp2);
		//cp.replaceCNF(c);
		

		// for(int i=0; i<c.cls.size(); i++){
			// for(int m=0; m<c.cls[i].lits.size(); m++){
				// cout<<c.cls[i].lits[m].from<<c.cls[i].lits[m].to<<" ";
			// }
			// cout<<"\n";
		// }

	}
	// cout<<"secondpartialCNF done\n";
	return ret;
}

cnf firstpartialCNF(string s){
	
	unordered_map<string,vector<string>>::const_iterator got;
	got = eligible.find(s);
	vector<string> v = got->second;
	cnf ret,arg;
	for(int j=0; j<v.size(); j++){
		//System.out.println(v.get(j));
		clause c;
		literal l = {s,v[j],true};
		c.lits.push_back(l);
		c.litsg2.insert(v[j]);
		arg.cls.push_back(c);
	}
	
	cnf spcnf = secondpartialCNF(1,arg);
	ret.cls.swap(spcnf.cls);
	/*
	for(int i=0; i<ret.cls.size(); i++){
		for(int j=0; j<ret.cls[i].lits.size(); j++){
			cout<<ret.cls[i].lits[j].from<<ret.cls[i].lits[j].to<<" ";
		}
		cout<<"\n";
	}*/
	cout<<"firstpartialCNF done\n";
	
	return ret;
}

//completing partially filled clause
void completeClause(clause c){
	
	for(int i=0; i<c.lits.size(); i++){
		for(int j=0; j<c.lits.size(); j++){
			if(i<j){
				
				string ifrom = c.lits[i].from; //A
				string ito = c.lits[i].to;  //P
				string jfrom = c.lits[j].from; //B
				string jto = c.lits[j].to; //Q
				
				if(ito==jto){
					return;
				}
			}
		}
	}
	
	for(int i=0; i<c.lits.size(); i++){
		for(int j=0; j<c.lits.size(); j++){
			if(i<j){
				
				string ifrom = c.lits[i].from; //A
				string ito = c.lits[i].to;  //P
				string jfrom = c.lits[j].from; //B
				string jto = c.lits[j].to; //Q
				
				clause cnew1;
				literal l1 = {ifrom,jfrom,true};
				literal l2 = {ito,jto,false};
				
				vector<literal> temp1(c.lits);
				cnew1.lits.swap(temp1);
				cnew1.lits.push_back(l1);
				cnew1.lits.push_back(l2);
				
				basicCNF.cls.push_back(cnew1);
				
				clause cnew2;
				literal l3 = {ifrom,jfrom,false};
				literal l4 = {ito,jto,true};
				
				vector<literal> temp2(c.lits);
				cnew2.lits.swap(temp2);
				cnew2.lits.push_back(l3);
				cnew2.lits.push_back(l4);
				
				basicCNF.cls.push_back(cnew2);
				
				clause cnew3;
				literal l5 = {jfrom,ifrom,true};
				literal l6 = {jto,ito,false};
				
				vector<literal> temp3(c.lits);
				cnew3.lits.swap(temp3);
				cnew3.lits.push_back(l5);
				cnew3.lits.push_back(l6);
				
				basicCNF.cls.push_back(cnew3);
				
				clause cnew4;
				literal l7 = {jfrom,ifrom,false};
				literal l8 = {jto,ito,true};
				
				vector<literal> temp4(c.lits);
				cnew4.lits.swap(temp4);
				cnew4.lits.push_back(l7);
				cnew4.lits.push_back(l8);
				
				basicCNF.cls.push_back(cnew4);
			}
		}
	}
}

//total cnf 
void constraintCNF() {
	
	//cnf pCNF = partialCNF();
	
	cnf pCNF = firstpartialCNF(vertexG1[0]);
	cout<<"pCNFsize "<<pCNF.cls.size()<<"\n";
	
	for(int i=0; i<pCNF.cls.size(); i++){
		completeClause((pCNF.cls[i]));
	}
}

//ensuring everyone gets mapped xor
void getMap() {
	unordered_map<string,vector<string>>::const_iterator got;
	
	
	/*
	cout << "eligible's buckets contain:\n";
	for ( unsigned i = 0; i < eligible.bucket_count(); ++i) {
		cout << "bucket #" << i << " contains:";
		for ( auto local_it = eligible.begin(i); local_it!= eligible.end(i); ++local_it ) {
			cout << " " << local_it->first << " - ";
			vector<string> v = local_it->second;
			for(int j=0;j<v.size();j++)
				cout<<v[j]<<", ";
		}
		std::cout << std::endl;
	}
	
	cout << "\n\neligibleg2's buckets contain:\n";
	for ( unsigned i = 0; i < eligibleg2.bucket_count(); ++i) {
		cout << "bucket #" << i << " contains:";
		for ( auto local_it = eligibleg2.begin(i); local_it!= eligibleg2.end(i); ++local_it ) {
			cout << " " << local_it->first << " - ";
			vector<string> v = local_it->second;
			for(int j=0;j<v.size();j++)
				cout<<v[j]<<", ";
		}
		std::cout << std::endl;
	}
	
	
	*/
	
	
	for(int i=0; i<vertexG1.size(); i++){
		clause c;
		string from = vertexG1[i];
		
		got = eligible.find(from);
		vector<string> v = got->second;
		
		
		
		for(int j=0; j<v.size(); j++){
			// System.out.print(v.get(j) + " ");
			string to = v[j];
			literal l1 = {from,to,false};
			c.lits.push_back(l1);
		}
		basicCNF.cls.push_back(c);
	}
 
	for(int j=0; j<vertexG1.size();j++){
		string from = vertexG1[j];
		got = eligible.find(from);
		vector<string> v = got->second;
		
		for(int k=0; k<v.size();k++){
			string to = v[k];
				
			for(int m=k+1; m<v.size(); m++){
				clause c;
				string tom = v[m];
				literal l1 = {from,to,true};
				literal l2 = {from,tom,true};
				c.lits.push_back(l1);
				c.lits.push_back(l2);
				basicCNF.cls.push_back(c);
			}	
		}
	}
	
	for(int j=0; j<vertexG2.size();j++){
		string to = vertexG2[j];
		got = eligibleg2.find(to);
		vector<string> v = got->second;
		
		for(int k=0; k<v.size();k++){
			string from = v[k];
				
			for(int m=k+1; m<v.size(); m++){
				clause c;
				string fromm = v[m];
				literal l1 = {from,to,true};
				literal l2 = {fromm,to,true};
				c.lits.push_back(l1);
				c.lits.push_back(l2);
				basicCNF.cls.push_back(c);
			}		
		}
	}
	
	unordered_map<string,vector<string>>::const_iterator got1;
	
	for(int i=0; i<vertexG1.size(); i++){
		string ifrom = vertexG1[i]; //A
		for(int j=0; j<vertexG1.size(); j++){
			if(i<j){
				got = eligible.find(vertexG1[i]);
				vector<string> v1 = got->second;
				
				string jfrom = vertexG1[j]; //B
				for(int k=0; k<v1.size(); k++){
					got1 = eligible.find(vertexG1[j]);
					vector<string> v2 = got1->second;
					
					string ito = v1[k];  //P
					for(int m=0; m<v2.size(); m++){
						string jto = v2[m]; //Q
						if(ito!=jto){
							
							
							clause cnew1;
							literal l1 = {ifrom,jfrom,true}; //~AB
							literal l2 = {ito,jto,false};   //PQ
							literal l3 = {ifrom,ito,true};   //~AP
							literal l4 = {jfrom,jto,true};   //~BQ
							
							cnew1.lits.push_back(l1);
							cnew1.lits.push_back(l2);
							cnew1.lits.push_back(l3);
							cnew1.lits.push_back(l4);
							
							basicCNF.cls.push_back(cnew1);
							
							clause cnew2;
							literal l5 = {ifrom,jfrom,false}; //AB
							literal l6 = {ito,jto,true};  //~PQ
							
							cnew2.lits.push_back(l3);
							cnew2.lits.push_back(l4);
							cnew2.lits.push_back(l5);
							cnew2.lits.push_back(l6);
							
							basicCNF.cls.push_back(cnew2);
							
							clause cnew3;
							literal l7 = {jfrom,ifrom,true};
							literal l8 = {jto,ito,false};
							
							cnew3.lits.push_back(l3);
							cnew3.lits.push_back(l4);
							cnew3.lits.push_back(l7);
							cnew3.lits.push_back(l8);
							
							basicCNF.cls.push_back(cnew3);
							
							clause cnew4;
							literal l9 = {jfrom,ifrom,false};
							literal l10 = {jto,ito,true};
							
							cnew4.lits.push_back(l3);
							cnew4.lits.push_back(l4);
							cnew4.lits.push_back(l9);
							cnew4.lits.push_back(l10);
							
							basicCNF.cls.push_back(cnew4);
							
							
						}
					}
				}
				
				
			}
		}
	}
	
	 for(int i=0; i<basicCNF.cls.size(); i++) {
		 for(int j=0;j<basicCNF.cls[i].lits.size();j++) {
			 cout<<basicCNF.cls[i].lits[j].from <<basicCNF.cls[i].lits[j].to <<basicCNF.cls[i].lits[j].isneg<<" ";
		 }
		 cout<<endl;
	 }
}


void eligibleMaps() {
	unordered_map<string,tupl>::const_iterator got;
	
	for(int i=0; i<vertexG1.size(); i++){
		string s = vertexG1[i];
		
		got = isVpresent.find(s);
		int indegree = (got->second).indegree;
		int outdegree = (got->second).outdegree;
		
		for(int j=0; j<vertexG2.size(); j++){
			string s2 = vertexG2[j];
			got = isVpresent.find(s2);
			int indegree2 = (got->second).indegree;
			int outdegree2 = (got->second).outdegree;
			
			if(indegree<=indegree2 && outdegree<=outdegree2){
				auto got1 = eligible.find(s);
				(got1->second).push_back(s2);
				auto got2 = eligibleg2.find(s2);
				(got2->second).push_back(s);
			}
		}
	}
}

void noMap(){
	
	if(vertexG1.size()>vertexG2.size() || edgeG1.size()>edgeG2.size()){
		ofstream fout("satinput");
	    ofstream foute("encoding");
	
	    foute<<"";
	
	    fout<<"p cnf "<<0<<" "<<0;

	    fout.close();
	    foute.close();
	    exit(0);
	}
	
	unordered_map<string,vector<string>>::const_iterator got;
	
	for(int i=0; i<vertexG1.size(); i++){

		string from = vertexG1[i];
		
		got = eligible.find(from);
		vector<string> v = got->second;
		
		if(v.size()==0){
				//write output to the file
	        ofstream fout("satinput");
	        ofstream foute("encoding");
	
	        foute<<"";
	
	        fout<<"p cnf "<<0<<" "<<0;

	        fout.close();
	        foute.close();
			exit(0);
		}
		
	}
}

bool wayToSort(string i, string j) {
	 
	 auto got = eligibleg2.find(i);
	 auto got1 = eligibleg2.find(j);
	 
	 // cout<<"\n\n"<<(got->second).size()<<"  "<<(got1->second).size();
	 return (got->second).size() < (got1->second).size();
}

void heuristic(){
	int num = vertexG2.size()*edgeG1.size();
	int den = vertexG1.size()*edgeG2.size();
	
	// int hvalue = num/den + 2;
	int hvalue = 8;
	
	for(int i=0; i<vertexG1.size(); i++){
		auto got = eligible.find(vertexG1[i]);
		vector<string> v = got->second;
		sort(v.begin(), v.end(), wayToSort);
		cout<<"\n\n";
		for(int a=0;a<v.size();a++)
			cout<<v[a]<<" ";
		vector<string> vnew;
		// cout<<hvalue<<"  "<<v.size();
		if(hvalue<v.size()) {
			for(int j=0; j<hvalue; j++){
				vnew.push_back(v[j]);
			}
		}
		else {
			for(int j=0; j<v.size(); j++){
				vnew.push_back(v[j]);
			}
		}
		got->second = vnew;
	}
	
	for(int i=0; i<vertexG2.size(); i++) {
		auto got = eligibleg2.find(vertexG2[i]);
		vector<string> vnew;
		got->second = vnew;
	}
	
	for(int i=0; i<vertexG1.size(); i++) {
		
		auto got = eligible.find(vertexG1[i]);
		vector<string> v = got->second;
		
		for(int j=0; j<v.size(); j++) {
			auto got2 = eligibleg2.find(v[j]);
			got2->second.push_back(vertexG1[i]);
		}
		
	}
	// for(int i=0; i<eligible.size();i++) {
		// auto got = eligible.find(vertexG1[i]);
		// vector<string> v = got->second;
		// for(int j=0;j<v.size();j++){
			// cout<<v[j]<<" ";
		// }
		// cout<<"\n\n";
	// }
	
	//sort on the basis of heuristicg2 values of g2 vertices and delete other than first hvalue elements from eligible g2 vertices
}
