import java.util.Vector;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Hashtable;
import java.io.PrintWriter;
public class finaloutput{
	
	public static Hashtable<Integer,String> encoding = new Hashtable<Integer,String>();  
	public static Vector<String> decoding = new Vector<String>();
	public static boolean exist=true;
	public static Integer symbols=0;
	
	public static void main(String args[]){
		
		//reading input
		
		try (BufferedReader br = new BufferedReader(new FileReader("encoding"))) {
			
            String line;
            
            while ((line = br.readLine()) != null) {
				if(line.equals("")){
					exist = false;
					break;
				}
                symbols++;
                String g1;
                String g2;
                boolean isg1 = true;
                
                String[] mapcode = new String[2];
                mapcode = line.split(" ");
                
                if(mapcode[0].substring(0,1).equals("s")){
					
					encoding.put(Integer.parseInt(mapcode[1]),mapcode[0]);
                    System.out.println(mapcode[1] + " " + mapcode[0]);
				}
                
                
                
            }
            
            
        }catch(Exception e){
			System.out.println("encoding File reading exception");
		}
		
		
		try (BufferedReader br = new BufferedReader(new FileReader("satoutput"))) {
			
            String line;
            
            line = br.readLine();
            if(line.equals("UNSAT")){
				exist = false;
			}
            
            while ((line = br.readLine()) != null) {
                
                if(line.equals(" 0")){
					exist = false;
					break;
				}
                
                String[] mapcode = new String[symbols+1];
                mapcode = line.split(" ");
                
                for(int i=0; i<mapcode.length-1; i++){
					if(Integer.parseInt(mapcode[i])>0){
						
				        String g;
				        
				        if(encoding.get(Integer.parseInt(mapcode[i]))!=null){
							g = encoding.get(Integer.parseInt(mapcode[i]));
						}else{
							continue;
						}
				        
				        boolean isg1edge = false;
				        for(int j=1; j<g.length(); j++){
							if(g.substring(j,j+1).equals("s")){
								isg1edge = true;
							}
						}
				        
						if(isg1edge==false){
							System.out.println(g);
							decoding.add(g);
						}
						
						
					}
				}
                
                
            }
            
            
        }catch(Exception e){
			System.out.println( e + " satinput File reading exception");
		}
		
		System.out.println(decoding.size());
		
		try{
		    PrintWriter writer = new PrintWriter("finaloutput");

            if(exist==false){
				writer.print("0");
				return;
			}
            
		    for(int i=0; i<decoding.size(); i++){
				
				String[] g1g2 = decoding.get(i).split("b");
				
				writer.println(g1g2[0].substring(1) + " " + g1g2[1]);
			}
		    
		    writer.close();

	    }catch(Exception e){
			System.out.println("File writing exception");
		}
		
	}
}
